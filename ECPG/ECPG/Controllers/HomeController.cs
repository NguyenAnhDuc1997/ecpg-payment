﻿using HppApi;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECPG.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
         {
            return View();
        }
        [Route("Index")]
        [HttpPost]
        public ActionResult Index(string merchantId, string terminalId, string transmode, string installment, string transamt, string notifyURL)
        {
            String orderid = "EC" + DateTime.Now.Ticks;
            ApiClient apiClient = new ApiClient();
            apiClient.setMERCHANTID(merchantId);
            apiClient.setTERMINALID(terminalId);
            apiClient.setORDERID(orderid);
            apiClient.setTRANSMODE(transmode);
            apiClient.setINSTALLMENT(installment);
            apiClient.setTRANSAMT(transamt);
            apiClient.setNotifyURL(notifyURL);
            apiClient.setURL("nccnet-ectest.nccc.com.tw", "/merchant/HPPRequest");
            var begin = DateTime.Now.Ticks;
            var rtnCode = apiClient.postTransaction();
            apiClient.log("Transaction KEY=" + apiClient.getKEY());
            var end = DateTime.Now.Ticks;
            apiClient.log("ORDERID=" + orderid + " RESPONSECODE=" + apiClient.getRESPONSECODE() + " RESPONSEMSG=" + apiClient.getRESPONSEMSG() + " " + ((end - begin) / 10000000.0) + " seconds used");
            apiClient.log("Transaction KEY=" + apiClient.getKEY());
            var responseCode = apiClient.getRESPONSECODE();
            if ("00".Equals(responseCode))
            { //Operation successful
                string key = apiClient.getKEY(); //Transaction key
                ViewBag.Key = apiClient.UrlEncode(key);
                return View();
            }
            else
            {
                ViewBag.Error = "Transaction is not successful. Please check your parameter again !!!!";
                return View();
            }
        }
        [Route("Notify")]
        public ActionResult Notify(string KEY)
        {
            ViewBag.KEY = KEY;
            return View();
        }
    }
}